1. SUMMARY

  The Beanstream Gateway module provides integration with Ubercart 2.x and UC
  Recurring Payments and Subscriptions.

  Implemented transaction types:
  * Standard Purchase
    Beanstream supports online credit card processing for Visa, MasterCard,
    American Express, Diners, Discover, JCB and Sears.  While the Beanstream
    system can handle all of these cards, merchants must acquire e-commerce
    merchant accounts for each card type they wish to accept on their website.

  * Pre-Authorization
    Pre-authorizations (PA) are often used instead of purchase transactions as a
    method of reducing the risks associated with credit card processing. When
    you process a pre-authorization, a temporary hold is placed on the customer
    card.  Merchants can then review customer-submitted data and identify high
    risk situations before processing the final pre-authorization completion
    transaction that will appear on a customer card statement.

  * Pre-Authorization Completion
    A Pre-Authorization Completion (PAC) is the second part of a
    pre-authorization.

  Recurring Payments allows you to process credit card transactions and
  Recurring Billing allows merchants to capture a customer’s transaction
  information a single time and automate future billing according to a regular
  schedule. Beanstream merchants benefit from automated email reports, detailed
  transaction histories, simple account creation and fully-flexible schedules.

  All recurring transactions are handled by Beanstream so there is no need to
  store credit card data. When Beanstream processes a transaction, the report
  will be sent to your site and the related order and recurring fee will be
  updated to reflect the new transaction.


2. REQUIREMENTS
	
  * A Beanstream account for credit card processing services and a compatable
  * merchant account. - http://beanstream.com/ Ubercart 2.x -
  * http://drupal.org/project/ubercart (optional) UC Recurring Payments and
  * Subscriptions - http://drupal.org/project/uc_recurring


3. INSTALLATION

   Install as usual, see http://drupal.org/node/70151 for further information.


4. CONFIGURATION

  Edit the settings in Store Administration > Configuration > Payment Settings >
  Payment Gateways.  In the Beanstream Gateway section there are several
  options:

  * [radio] Default Credit Transaction Type

    * Authorization Only
      If you choose this option, you will need to do the following in the
      Beanstream member area.  Log in to the Beanstream member area and navigate
      to administration > account admin > order settings in the left menu. Under
      the heading “Restrict Internet Transaction Processing Types”, de-select the
      “Restrict Internet Transaction Processing Types” checkbox to allow you to
      process all types of transactions including purchase, authm and pre-auth
      completions.

    * Authorize and Capture Immediately
      Choosing this option will have all transactions immediately processed when
      sent to Beanstream. This is Beanstream's default behavior. If this is all
      you need there is no need to change any settings in the Beanstream member
      area.

  * [checkbox] Development Mode
    When set to development, this will display warnings and transaction
    information. This should only be used while developing your site and never
    enabled in production.

  * [textfield] Merchant ID
    This is the merchant id that you are using with Beanstream.

  * [textfield] Hash Key (optional but recommended)
    Hash validation is used to help protect the integrity of API transaction
    requests. Log into the Beanstream member area (http://beanstream.com/) and
    navigate to Administration > Account Settings > Order Settings in the left
    menu.  Select "Use hash validation against transaction". Enter a Hash Key
    and copy and paste that value here.
	
  * [select] Hash Algorithm
    Beanstream supports MD5 or SHA-1 hash encryption. Log into the Beanstream
    member area and navigate to Administration > Account Settings > Order
    Settings in the left menu. Select your preferred Hash Algorithm. This
    setting must match what you selected at Beanstream. SHA-1 is recommended
    over MD5.

  * [textfield] Recurring Billing API Key (required for recurring payments)
    Before modification requests can be processed, you must generate a unique
    recurring billing API passcode.  Log into the Beanstream member area and
    navigate to administration > account settings > order settings in the left
    menu. Generate the API Key and copy and paste that value here.


5. CONTACT

  Maintainers:
  * Andrew Hawthorne (hawthorne) - http://drupal.org/user/1341246
