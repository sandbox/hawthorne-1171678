<?php
// $Id$

/**
 *  @file
 *  uc_recurring implementation for the beanstream gateway module
 */

/*******************************************************************************
 * Hooks
 ******************************************************************************/

/**
 * Implements hook_recurring_info().
 *
 * The 'charge' menu item has been disabled because there is no support for it in Beanstream.
 * We will use the default edit page and alter it.
 */
function uc_beans_recurring_info() {
  $items[UC_BEANS_GATEWAY_ID] = array(
    'name'               => t('Beanstream Gateway (Recurring)'),
    'payment method'     => 'credit',
    'module'             => 'uc_beans',
    'fee handler'        => UC_BEANS_GATEWAY_ID,
    'process callback'   => 'uc_beans_recurring_process',
    'cancel callback'    => 'uc_beans_recurring_cancel',
    'renew callback'     => 'uc_beans_recurring_renew',
    'saved profile'      => TRUE,
    'own handler'        => TRUE, // renewals are processed by Beanstream directly
    'menu'               => array(
      'charge'             => UC_RECURRING_MENU_DISABLED,
      'edit'               => UC_RECURRING_MENU_DEFAULT,
      'cancel'             => UC_RECURRING_MENU_DEFAULT,
    ), // Use the default user operation defined in uc_recurring.
  );
  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter() for uc_recurring_admin_edit_form()
 */
function uc_beans_form_uc_recurring_admin_edit_form_alter(&$form, &$form_state) {
  $rfid = $form['rfid']['#value'];
  $fee = uc_recurring_fee_user_load($rfid);

  $form['fee_amount']['#default_value'] = uc_currency_format($fee->fee_amount, FALSE, FALSE, '.');

  unset($form['next_charge']['next_charge_time']);
  unset($form['reset_next_charge']);

  $form['next_charge']['#description'] = t('This is when the next billing cycle begins.');
  $form['next_charge']['next_charge_date'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Date'),
    '#default_value' => date('Y-m-d', $fee->next_charge),
    '#disabled'      => TRUE,
  );

  // add submit handler to post to beanstream
  $form['#submit'][] = '_uc_beans_recurring_edit_submit';
  $form['#submit'] = array_reverse($form['#submit']);
}

/*******************************************************************************
 * Callbacks
 ******************************************************************************/

/**
 * Process Callback defined in uc_beans_recurring_info
 *
 * @param $order
 *   An array representing an order
 * @param &$fee
 *   An object, passed by reference, representing a recurring fee
 *
 * @return
 *   TRUE on successful creation of a recurring fee
 *   FALSE on any failure
 */
function uc_beans_recurring_process($order, &$fee) {
  $data = array(
    'txn_type' => UC_CREDIT_REFERENCE_SET,
    'fee'      => array(
      'fee_amount'          => $fee->fee_amount,
      'next_charge'         => $fee->next_charge,
      'initial_charge'      => $fee->initial_charge,
      'regular_interval'    => $fee->regular_interval,
      'remaining_intervals' => $fee->remaining_intervals,
      'order_product_id'    => $fee->order_product_id,
      'nid'                 => $fee->data['nid'],
    )
  );

  $result = uc_beans_txn_route($order->order_id, $order->total_amount, $data);

  // update the fee with details returned from uc_beans_txn_route
  if (isset($result['fee_update'])) {
    foreach ($result['fee_update'] as $key => $val) {
      if (is_array($val) && is_array($fee->$key)) {
        $merged = array_merge($fee->$key, $val);
        $fee->$key = $merged;
      }
      else {
        $fee->$key = $val;
      }
    }
  }

  // returns true on transaction success
  return $result['success'];
}

/**
 * Cancel Callback defined in uc_beans_recurring_info
 *
 * @param $fee
 *   An object representing a recurring fee
 *
 * @return void
 */
function uc_beans_recurring_cancel($fee) {
  if ($fee->fee_handler == UC_BEANS_GATEWAY_ID) {
    $update = array(
      // Cancel the account
      'operationType' => 'C',
      'rbAccountId'   => $fee->data['rbAccountId'],
    );
    // prepare request
    $request = _uc_beans_recurring_prepare_request($update);

    // submit request and receive xml response
    $response = new SimpleXMLElement(_uc_beans_send_request($request, UC_BEANS_MODIFY_RECURRING_URL));

    _uc_beans_process_xml_response($response);
  }
}

/**
 * Form submission handler for uc_recurring_admin_edit_form()
 *
 * @see uc_recurring_admin_edit_form_validate()
 */
function _uc_beans_recurring_edit_submit($form, &$form_state) {
  $rfid = $form_state['values']['rfid'];
  $fee = uc_recurring_fee_user_load($rfid);

  if ($fee->fee_handler == UC_BEANS_GATEWAY_ID) {
    $update = array(
      'operationType' => 'M',
      'rbAccountId'   => $fee->data['rbAccountId'],
    );

    $interval = $form_state['values']['regular_interval_value'] .' '. $form_state['values']['regular_interval_unit'];
    $number_intervals = $form_state['values']['number_intervals'];

    // interval
    if ($fee->regular_interval != $interval) {
      $update['rbBillingIncrement'] = $form_state['values']['regular_interval_value'];
      $update['rbBillingPeriod'] = strtoupper(substr($form_state['values']['regular_interval_unit'], 0, 1));

      drupal_set_message(t('The new billing interval will come in to effect after the next charge completes.'));
    }

    // next charge
    $next_pay_date = date('Y-m-d', $fee->next_charge);
    $form_state['values']['next_charge_date'] = array(
      'year'  => date('Y', strtotime($next_pay_date)),
      'month' => date('n', strtotime($next_pay_date)),
      'day'   => date('j', strtotime($next_pay_date)),
    );

    // expires
    if ($fee->remaining_intervals == UC_RECURRING_UNLIMITED_INTERVALS && empty($form_state['values']['unlimited_intervals']) ) {
      $update['rbNeverExpires'] = 0;
    }
    elseif ($fee->remaining_intervals != UC_RECURRING_UNLIMITED_INTERVALS && 1 == $form_state['values']['unlimited_intervals']) {
      $update['rbNeverExpires'] = 1;
    }

    // expire date
    if ((!empty($number_intervals) && $fee->remaining_intervals != $number_intervals) || (!empty($number_intervals) && $fee->regular_interval != $interval)) {
      $expire_interval = ($number_intervals * $form_state['values']['regular_interval_value']) . ' ' . $form_state['values']['regular_interval_unit'];
      $update['rbExpiry'] = date(UC_BEANS_API_DATE_FORMAT, strtotime($next_pay_date . ' + ' . $expire_interval));
    }

    // amount
    if ($fee->fee_amount != $form_state['values']['fee_amount']) {
      // uc_currency_format was returning '0.00'...? so preg_replace was used instead. Beanstream requires amount to be max 9 digits in the form of 0.00
      $update['Amount'] = preg_replace('/[^0-9\.]+/', '', $form_state['values']['fee_amount']);
    }

    if (2 < count($update)) {
      // prepare request
      $request = _uc_beans_recurring_prepare_request($update);

      // submit request and receive xml response
      $response = new SimpleXMLElement(_uc_beans_send_request($request, UC_BEANS_MODIFY_RECURRING_URL));

      _uc_beans_process_xml_response($response);

    }
  }
}

/**
 * Renew Callback defined in uc_beans_recurring_info
 */
function uc_beans_recurring_renew($order, &$fee) {
  // log transaction
  $message = t('Status: @status<br/>Amount: @amount<br/>Transaction Type: @txn_type<br/>Transaction ID: @txn_id<br/>Auth Code: @auth_code<br/>Period From: @period_from<br/>Period To: @period_to',
    array(
      '@status'      => $fee->trn_data['messageText'],
      '@auth_code'   => $fee->trn_data['authCode'],
      '@amount'      => uc_currency_format($fee->trn_data['billingAmount']),
      '@txn_type'    => 'Recurring',
      '@card_type'   => _uc_beans_card_type($fee->trn_data['cardType']),
      '@txn_id'      => $fee->trn_data['trnId'],
      '@period_from' => $fee->trn_data['periodFrom'],
      '@period_to'   => $fee->trn_data['periodTo'],
    )
  );
  uc_order_comment_save($order->order_id, 0, $message, 'admin');

  // recurring details can be updated at beanstream so make sure that the recurring fee is up to date with the correct interval
  // next_charge = periodFrom + interval (needs to be a timestamp)
  $bill_period = array(
    'D' => 'days',
    'W' => 'weeks',
    'M' => 'months',
    'Y' => 'years'
  );
  $bill_interval = $fee->trn_data['billingIncrement'] . ' ' . $bill_period[$fee->trn_data['billingPeriod']];

  $fee->next_charge = strtotime($fee->trn_data['billingDate'] . ' +' . $bill_interval);
  $fee->regular_interval = $bill_interval;

  if (1 == $fee->trn_data['trnApproved']) {
    uc_payment_enter($order->order_id, $order->payment_method, $order->order_total, $fee->uid, $result['data'], $result['comment']);
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Page handler for beanstream/report-process
 *
 * Beanstream can send HTTP POST reports to a specified URL whenever a recurring transaction has been processed.
 * The url provided to beanstream will be http://example.com/beanstream/report-process.
 */
function uc_recurring_report_process() {
  global $user;
  $process_failure = FALSE;

  $filtered_input = filter_var_array($_POST, uc_beans_recurring_report_process_filter());
  // log the filtered input
  watchdog('uc_beans', 'Recieved POST: <pre>@data</pre>', array('@data' => var_export($filtered_input, TRUE)), WATCHDOG_NOTICE);

  // none of the posted values should be FALSE. If there are FALSE values then the POST has failed the filter.
  if (FALSE !== $failed = array_search(NULL, $filtered_input, TRUE)) {
    $process_failure = TRUE;
    $failure = array(
      'message' => t('One or more input fields failed validation. Filtered input: <pre>@filtered</pre>'),
      'data'    => array('@filtered' => var_export($filtered_input, TRUE))
    );
  }

  // only proceed with no validation failures
  if (FALSE === $process_failure) {

    // ref1 should be encrypted, so decrypt it
    if ($key = uc_credit_encryption_key()) {

      // setup encryption object
      $crypt = new uc_encryption_class;
      $filtered_input['ref1'] = $crypt->decrypt($key, $filtered_input['ref1']);

      // store encryption errors
      uc_store_encryption_errors($crypt, 'uc_beans');
    }
    if (FALSE === ($filtered_input['ref1'] = unserialize($filtered_input['ref1']))) {
      $process_failure = TRUE;
      $failure = array(
        'message' => t('Reference values could not be unserialized.'),
        'data'    => array()
      );
    }
    // these values MUST be stored with the recurring account at beanstream, otherwise we can't process the report.
    elseif (empty($filtered_input['ref1']['order_id']) || empty($filtered_input['ref1']['order_product_id']) || empty($filtered_input['ref1']['nid'])) {
      $process_failure = TRUE;
      $failure = array(
        'message' => t('Required reference values could not be found to create new recurring order.'),
        'data'    => array()
      );
    }
  }

  // only proceed if ref1 was successfully decrypted and unserialized
  if (FALSE === $process_failure) {
    // renewals processed via uc_recurring_renew() this will need to be written for beanstream

    // load the fee
    $fee = db_fetch_object(db_query("SELECT * FROM {uc_recurring_users} WHERE order_product_id = %d", $filtered_input['ref1']['order_product_id']));

    // append transaction data to the fee object. This will be handled through the renew handler.
    $fee->trn_data = $filtered_input;
    $fee->data = unserialize($fee->data);

    // create the new recurring order and return the new order id
    $success = uc_recurring_renew($fee);

    if ($success) {
      // @todo old_order is not getting the correct value
      watchdog('uc_beans', t('Order @new_order created as a recurring fee for order @old_order'), array('@new_order' => $success, '@old_order' => $fee->trn_data['order_id']), WATCHDOG_NOTICE);
    }
    else {
      watchdog('uc_beans', t('There was an error processing a recurring fee. View order @new_order for details'), array('@new_order' => $success), WATCHDOG_ERROR);
    }
  }

  if ($process_failure && !empty($failure)) {
    watchdog('uc_beans', $failure['message'], $failiure['data'], WATCHDOG_ERROR);
  }

  return NULL;
}

/**
 * Filter array to validate input from beanstream http post report
 *
 * @return
 *   a filter array
 */
function uc_beans_recurring_report_process_filter() {
  $date_regex = '/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/';

  $beans_filter = array(
    'billingId' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/[0-9]+/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'trnApproved' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/^[01]$/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'trnId' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/[0-9]+/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'messageId' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/^[0-9]{1,3}$/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'messageText' => array(
      'filter' => FILTER_SANITIZE_STRING,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'authCode' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/(TEST)|[0-9]+/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'accountName' => array(
      'filter' => FILTER_SANITIZE_STRING,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'emailAddress' => FILTER_VALIDATE_EMAIL,
    'billingAmount' => array(
      'filter' => FILTER_VALIDATE_FLOAT,
      'flags'  => FILTER_FLAG_ALLOW_THOUSAND
     ),
    'billingDate' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => $date_regex),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'billingPeriod' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/^[DWMY]$/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'billingIncrement' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => '/[0-9]+/'),
      'flags'   => FILTER_NULL_ON_FAILURE
    ),
    'periodFrom' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => $date_regex)
    ),
    'periodTo' => array(
      'filter'  => FILTER_VALIDATE_REGEXP,
      'options' => array('regexp' => $date_regex)
    ),
    'orderNumber' => array(
      'filter' => FILTER_SANITIZE_STRING,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'ref1' => array(
      'filter' => FILTER_UNSAFE_RAW,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'ref2' => array(
      'filter' => FILTER_UNSAFE_RAW,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'ref3' => array(
      'filter' => FILTER_UNSAFE_RAW,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'ref4' => array(
      'filter' => FILTER_UNSAFE_RAW,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    ),
    'ref5' => array(
      'filter' => FILTER_UNSAFE_RAW,
      'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    )
  );

  return $beans_filter;
}

/*******************************************************************************
 * Internal
 ******************************************************************************/

/**
 * Prepare recurring account request query string
 *
 * @param $data
 *   An associative array contain data to be appended to the recurring request
 *
 * @return
 *   a key-value query string (key=value&key2=value2)
 */
function _uc_beans_recurring_prepare_request($data) {
  $request_data = array(
    'merchantId'     => _uc_beans_key_decrypt('uc_beans_merchant_id'),
    'serviceVersion' => '1.0',
    'operationType'  => $data['operationType'],
    'passcode'       => _uc_beans_key_decrypt('uc_beans_recurring_api_key'),
    'rbAccountId'    => $data['rbAccountId']
  );

  // add data to request
  foreach ($data as $key => $val) {
    $request_data[$key] = $val;
  }

  $request = _uc_beans_format_request_array($request_data);

  return $request;

}

/**
 * Handles creation of recurring billing account at Beanstream.
 *
 * Build a request query and submit it to Beanstream. Take the response
 * and determine if the request was successful.
 *
 * @param $order
 *   an object containing the order details
 * @param $data
 *   an array containing additional information for building
 *   the Beanstream request query.
 *
 * @return
 *   If Beanstream responds with trnApproved=1
 *    array('success' => TRUE, 'fee_update' => ARRAY)
 *   Otherwise
 *    array('success' => FALSE, 'message' => STRING)
 *
 */
function uc_beans_recurring_account_create($order, $data) {
  global $user;

  // recurring fee should not include any taxes applied to the order.
  $fee_update = array();

  // use ref1 to track the order id and the product id for reporting
  $recurring = array(
    'trnRecurring' => 1,
    'ref1'         => serialize(array(
      'order_id'         => $order->order_id,
      'order_product_id' => $data['fee']['order_product_id'],
      'nid'              => $data['fee']['nid'],
    )),
  );

  // encrypt the ref1 field
  if ($key = uc_credit_encryption_key()) {
    // Setup our encryption object.
    $crypt = new uc_encryption_class;

    $recurring['ref1'] = $crypt->encrypt($key, $recurring['ref1']);
  }

  // format regular_interval to Beanstream's rbBillingPeriod and rbBillingIncrement
  preg_match('/[ymwd]/i', $data['fee']['regular_interval'], $match);
  if (!empty($match)) {
    $recurring['rbBillingPeriod'] = strtoupper($match[0]);
  }

  preg_match('/[0-9]+/', $data['fee']['regular_interval'], $match);
  if (!empty($match)) {
    $recurring['rbBillingIncrement'] = $match[0];
  }

  // check order status. do not charge if the order status is 'payment_received', make sure rbFirstBilling is correctly set.
  if ('payment_received' == $order->order_status) {
    $fee_update['next_charge']       = strtotime('now + ' . $data['fee']['regular_interval']);
    $fee_update['charged_intervals'] = 1;
    $recurring['rbCharge']           = 0;
    $recurring['rbFirstBilling']     = date(UC_BEANS_API_DATE_FORMAT, $fee_update['next_charge']);
  }
  elseif ($data['fee']['next_charge'] > strtotime('now')) {
    // first bill in the future
    $recurring['rbCharge']        = 0;
    $recurring['rbFirstBilling']  = date(UC_BEANS_API_DATE_FORMAT, $data['fee']['next_charge']);
  }
  else {
    // bill now
    $recurring['rbCharge']           = 1;
    $fee_update['next_charge']       = strtotime('now + ' . $data['fee']['regular_interval']);
    $fee_update['charged_intervals'] = 1;
  }

  // build the request
  $request = _uc_beans_prepare_request($order, $order->order_total, $recurring, $data['txn_type']);

  if (_uc_beans_is_dev()) {
    drupal_set_message('<pre><b>REQUEST:</b> '. var_export($request, TRUE) .'</pre>');
  }

  // send curl request to beanstream to create the recurring billing account
  $response = _uc_beans_send_request($request, UC_BEANS_TXN_GATEWAY_URL);

  // parse response
  parse_str($response, $resp_info);

  if (_uc_beans_is_dev()) {
    drupal_set_message('<pre><b>RESPONSE:</b> '. var_export($resp_info, TRUE) .'</pre>');
  }

  // log order comment
  if ($resp_info['trnApproved'] != '1') {
    // failed
    uc_order_comment_save($order->order_id, $user->uid, t('Beanstream Gateway: Recurring account creation failed!<br/>Message: @message<br/>Transaction ID: @txn_id', array('@message' => $resp_info['messageText'], '@txn_id' => $resp_info['trnId'])), 'admin');
    return array('success' => FALSE, 'message' => 'Beanstream Gateway: Recurring account creation failed! Message: ' . $resp_info['messageText']);
  }
  else {
    // success
    $fee_update['data'] = array('rbAccountId' => $resp_info['rbAccountId']);
    uc_order_comment_save($order->order_id, $user->uid, t('Beanstream Gateway: Recurring account created!<br/>Message: @message<br/>Transaction ID: @txn_id<br/>Recurring Account ID: @rb_acct_id', array('@message' => $resp_info['messageText'], '@txn_id' => $resp_info['trnId'], '@rb_acct_id' => $resp_info['rbAccountId'])), 'admin');

    // save credit reference
    $order->data = uc_credit_log_reference($order->order_id, $resp_info['rbAccountId'], '');
    return array('success' => TRUE, 'fee_update' => $fee_update);
  }
}

/**
 * Recieves a SimpleXML object and sets a drupal message
 *
 * @param $response
 *   A SimpleXML object containing a beanstream response to a recurring account
 *   update request
 *
 * @return void
 */
function _uc_beans_process_xml_response($response) {
  if (empty($response->errors)) {
    $beans_message = array(
      'message' => 'Beanstream: ' . (string) $response->message,
      'type'    => 'status',
    );
  }
  else {
    $beans_message = array(
      'message' => 'Beanstream: ' . (string) $response->message . '<br/>' . (string) $response->errors->parameter->reason ,
      'type'    => 'error',
    );
  }

  drupal_set_message(t($beans_message['message']), $beans_message['type']);
}
